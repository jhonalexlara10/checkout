package com.codingchallenge.checkout.infrastructure.configuration;

import com.codingchallenge.checkout.domain.port.repository.LogisticRepository;
import com.codingchallenge.checkout.domain.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BeanService {
	@Bean
	public CreateLogisticService createLogisticService(LogisticRepository logisticRepository) {
		return new CreateLogisticService(logisticRepository);
	}
	@Bean
	public ConsultLogisticService consultLogisticService(LogisticRepository logisticRepository){
		return  new ConsultLogisticService(logisticRepository);
	}
}
