package com.codingchallenge.checkout.infrastructure.adapter.repository;

import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.model.Logistic;
import com.codingchallenge.checkout.domain.port.repository.LogisticRepository;
import com.codingchallenge.checkout.infrastructure.LogisticRepositoryJPA;
import com.codingchallenge.checkout.infrastructure.entity.LogisticEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;


@Repository
public class RepositoryLogisticH2 implements LogisticRepository {

	private final LogisticRepositoryJPA logisticRepositoryJPA;
	private ModelMapper modelMapper = new ModelMapper();

	public RepositoryLogisticH2(LogisticRepositoryJPA logisticRepositoryJPA) {
		this.logisticRepositoryJPA = logisticRepositoryJPA;

	}
	@Override
	public void save(Logistic logistic) {
		LogisticEntity logisticEntity = modelMapper.map(logistic, LogisticEntity.class);
		logisticRepositoryJPA.save(logisticEntity);
	}

	@Override
	public List<LogisticCommand> getLogistics() {
		List<LogisticEntity> listLogisticEntity = logisticRepositoryJPA.findAll();
		List<LogisticCommand> listLogistics = new ArrayList<>();
		for (LogisticEntity logisticEntity : listLogisticEntity) {
			LogisticCommand logistic = modelMapper.map(logisticEntity, LogisticCommand.class);
			listLogistics.add(logistic);
		}
		return listLogistics;
	}

}
