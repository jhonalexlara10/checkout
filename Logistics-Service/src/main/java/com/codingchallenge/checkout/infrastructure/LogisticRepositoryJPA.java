package com.codingchallenge.checkout.infrastructure;

import java.io.Serializable;

import com.codingchallenge.checkout.infrastructure.entity.LogisticEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;


@Repository
public interface LogisticRepositoryJPA extends JpaRepository<LogisticEntity, Serializable> {

}
