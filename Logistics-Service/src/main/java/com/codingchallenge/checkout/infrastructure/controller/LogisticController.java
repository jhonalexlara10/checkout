package com.codingchallenge.checkout.infrastructure.controller;

import java.time.LocalDate;
import java.util.List;

import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.application.handler.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/logistic")
public class LogisticController {
	private final HandlerLogistic handlerLogistic;
	private final HandlerConsulLogistics handlerConsulLogistics;

	@Autowired
	public LogisticController(HandlerLogistic handlerLogistic, HandlerConsulLogistics handlerConsulLogistics) {
		this.handlerLogistic = handlerLogistic;
		this.handlerConsulLogistics= handlerConsulLogistics;

	}
	@ApiOperation(value = "Create a new Logistic and get gelivery date", nickname = "saveLogistic", notes = "",response = LocalDate.class ,tags={ "logistic-controller", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully create a new logistic", response = LocalDate.class),
			@ApiResponse(code = 201, message = "Created"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Application failed to process the request") })
	@PostMapping("/saveLogistic")
	public LocalDate saveLogistic(@RequestBody LogisticCommand logisticCommand) {
		return handlerLogistic.execute(logisticCommand);
	}
	@ApiOperation(value = "Get the logistics", nickname = "getLogistics", notes = "",response = List.class,tags={ "logistic-controller", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully: get the logistics", response = List.class),
			@ApiResponse(code = 201, message = "Created"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Application failed to process the request") })
	@GetMapping("/getLogistics")
	public List<LogisticCommand> getLogistics() {
		return handlerConsulLogistics.execute();
	}
}
