package com.codingchallenge.checkout.infrastructure.entity;

import java.time.LocalDate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "LOGISTIC_ENTITY")
public class LogisticEntity {

	@Id
	@Column(name = "CLIENT_ID")
	private Long clientId;

	@Column(name = "DIRECTION")
	private String direction;

	@Column(name = "DATE")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate date;

	@Column(name = "DELIVERED_DATE")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate deliveredDate;

	public LogisticEntity(Long id, Long clientId, String direction, LocalDate date, LocalDate deliveredDate) {
		super();
		this.clientId = clientId;
		this.direction = direction;
		this.date = date;
		this.deliveredDate = deliveredDate;
	}

	public LogisticEntity() {
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalDate getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(LocalDate deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

}
