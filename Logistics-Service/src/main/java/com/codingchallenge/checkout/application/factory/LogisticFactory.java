package com.codingchallenge.checkout.application.factory;

import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.model.Logistic;
import org.springframework.stereotype.Component;


@Component
public class LogisticFactory {
	public Logistic createLogistic(LogisticCommand logisticCommand) {
		return new Logistic(logisticCommand.getClientId(), logisticCommand.getDirection(), logisticCommand.getDate(),
				logisticCommand.getDeliveredDate());
	}
}
