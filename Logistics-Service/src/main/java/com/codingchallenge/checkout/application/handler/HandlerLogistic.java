package com.codingchallenge.checkout.application.handler;

import java.time.LocalDate;


import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.application.factory.LogisticFactory;
import com.codingchallenge.checkout.domain.model.Logistic;
import com.codingchallenge.checkout.domain.service.CreateLogisticService;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class HandlerLogistic {
	private final CreateLogisticService createLogisticService;
	private final LogisticFactory logisticFactory;

	public HandlerLogistic(CreateLogisticService createLogisticService, LogisticFactory logisticFactory) {

		this.createLogisticService = createLogisticService;
		this.logisticFactory = logisticFactory;
	}

	@Transactional
	public LocalDate execute(LogisticCommand logisticCommand) {
		Logistic logistic = this.logisticFactory.createLogistic(logisticCommand);
		return createLogisticService.execute(logistic);
	}
}
