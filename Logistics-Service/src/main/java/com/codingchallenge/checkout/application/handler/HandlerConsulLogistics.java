package com.codingchallenge.checkout.application.handler;

import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.service.ConsultLogisticService;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class HandlerConsulLogistics {
    private final ConsultLogisticService consultLogisticService;

    public HandlerConsulLogistics(ConsultLogisticService consultLogisticService) {

        this.consultLogisticService = consultLogisticService;
    }

    @Transactional
    public List<LogisticCommand> execute() {
        return this.consultLogisticService.execute();
    }
}
