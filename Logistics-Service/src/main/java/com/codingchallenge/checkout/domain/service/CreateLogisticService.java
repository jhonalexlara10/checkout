package com.codingchallenge.checkout.domain.service;

import com.codingchallenge.checkout.domain.model.Logistic;
import com.codingchallenge.checkout.domain.port.repository.LogisticRepository;

import java.time.LocalDate;


public class CreateLogisticService {
	private LogisticRepository logisticReposiroty;

	public CreateLogisticService(LogisticRepository logisticReposiroty) {

		this.logisticReposiroty = logisticReposiroty;
	}

	public LocalDate execute(Logistic logistic) {
		LocalDate delivered = logistic.getDate().plusDays(7);
		logistic.setDeliveredDate(delivered);
		this.logisticReposiroty.save(logistic);
		return delivered;
	}

}
