package com.codingchallenge.checkout.domain.port.repository;


import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.model.Logistic;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface LogisticRepository {
	void save(Logistic logistic);
	List<LogisticCommand> getLogistics();
}
