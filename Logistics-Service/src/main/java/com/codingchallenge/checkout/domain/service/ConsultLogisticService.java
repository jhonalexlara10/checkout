package com.codingchallenge.checkout.domain.service;

import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.port.repository.LogisticRepository;

import java.util.List;

public class ConsultLogisticService {
    private LogisticRepository logisticRepository;

    public ConsultLogisticService(LogisticRepository logisticRepository) {

        this.logisticRepository = logisticRepository;
    }

    public List<LogisticCommand> execute() {
        return this.logisticRepository.getLogistics();
    }
}
