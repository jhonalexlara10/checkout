package com.codingchallenge.checkout.application.command;

import java.time.LocalDate;
import java.util.List;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "clientId")

public class OrderCommand {
	private Long clientId;
	private String direction;
	@JsonDeserialize(as = LocalDate.class)
	private LocalDate date;
	private List<ProductCommand> products;

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public List<ProductCommand> getProducts() {
		return products;
	}

	public void setProducts(List<ProductCommand> products) {
		this.products = products;
	}

	public OrderCommand(Long clientId, String direction, LocalDate date, List<ProductCommand> products) {
		super();
		this.clientId = clientId;
		this.direction = direction;
		this.date = date;
		this.products = products;
	}

	public OrderCommand() {
		super();
	}
}
