package com.codingchallenge.checkout.application.handler;

import javax.transaction.Transactional;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.OrderCommand;
import com.codingchallenge.checkout.application.factory.OrderFactory;
import com.codingchallenge.checkout.domain.model.Order;
import com.codingchallenge.checkout.domain.service.CreateCheckoutService;
import org.springframework.stereotype.Component;


@Component
public class HandlerCheckout {
	private final CreateCheckoutService createCheckoutService;
	private final OrderFactory orderFactory;

	public HandlerCheckout(CreateCheckoutService createCheckoutService, OrderFactory orderFactory) {

		this.createCheckoutService = createCheckoutService;
		this.orderFactory = orderFactory;
	}

	@Transactional
	public BillCommand ejecutar(OrderCommand orderCommand) {
		Order order = this.orderFactory.createCheckout(orderCommand);
		return createCheckoutService.execute(order);
	}
}
