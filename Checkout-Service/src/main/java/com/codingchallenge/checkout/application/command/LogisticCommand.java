package com.codingchallenge.checkout.application.command;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.time.LocalDate;

@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "clientId")

public class LogisticCommand {

	private Long clientId;
	private String direction;
	@JsonDeserialize(as = LocalDate.class)
	private LocalDate date;
	@JsonDeserialize(as = LocalDate.class)
	private LocalDate deliveredDate;

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalDate getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(LocalDate deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

	public LogisticCommand(Long clientId, String direction, LocalDate date, LocalDate deliveredDate) {
		super();
		this.clientId = clientId;
		this.direction = direction;
		this.date = date;
		this.deliveredDate = deliveredDate;
	}

	public LogisticCommand() {
	}

}
