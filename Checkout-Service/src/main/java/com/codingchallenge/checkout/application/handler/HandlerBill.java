package com.codingchallenge.checkout.application.handler;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.OrderCommand;
import com.codingchallenge.checkout.application.factory.BillFactory;
import com.codingchallenge.checkout.application.factory.OrderFactory;
import com.codingchallenge.checkout.domain.model.Bill;
import com.codingchallenge.checkout.domain.model.Order;
import com.codingchallenge.checkout.domain.service.CreateBillService;
import com.codingchallenge.checkout.domain.service.CreateCheckoutService;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;

@Component
public class HandlerBill {
    private final CreateBillService createBillService;
    private final BillFactory billFactory;

    public HandlerBill(CreateBillService createBillService, BillFactory billFactory) {
        this.billFactory=billFactory;
        this.createBillService = createBillService;
    }

    @Transactional
    public void execute(BillCommand billCommand) {
        Bill bill = this.billFactory.createBill(billCommand);
        this.createBillService.execute(bill);
    }
}
