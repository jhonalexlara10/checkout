package com.codingchallenge.checkout.application.factory;

import java.util.ArrayList;
import java.util.List;

import com.codingchallenge.checkout.application.command.OrderCommand;
import com.codingchallenge.checkout.application.command.ProductCommand;
import com.codingchallenge.checkout.domain.model.Order;
import com.codingchallenge.checkout.domain.model.Product;
import org.springframework.stereotype.Component;


@Component
public class OrderFactory {
	List<Product> products = new ArrayList<Product>();

	public Order createCheckout(OrderCommand orderCommand) {
		for (ProductCommand product : orderCommand.getProducts()) {
			products.add(new Product(product.getId(), product.getQuantity(), product.getCost()));
		}

		return new Order(orderCommand.getClientId(), orderCommand.getDate(), orderCommand.getDirection(), products);
	}
}
