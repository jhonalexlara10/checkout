package com.codingchallenge.checkout.application.handler;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.service.ConsultBillService;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.List;

@Component
public class HandlerConsultBill {
    private final ConsultBillService consultBillService;

    public HandlerConsultBill(ConsultBillService consultBillService) {

        this.consultBillService = consultBillService;
    }

    @Transactional
    public List<BillCommand> execute() {
        return this.consultBillService.execute();
    }

}
