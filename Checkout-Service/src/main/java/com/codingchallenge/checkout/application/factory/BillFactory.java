package com.codingchallenge.checkout.application.factory;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.model.Bill;
import com.codingchallenge.checkout.domain.model.Logistic;
import org.springframework.stereotype.Component;

@Component
public class BillFactory {
    public Bill createBill(BillCommand billCommand) {
        return new Bill(billCommand.getClientId(), billCommand.getDirection(), billCommand.getDeliveryDate(),
                billCommand.getCost());
    }
}
