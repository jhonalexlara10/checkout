package com.codingchallenge.checkout.application.command;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.time.LocalDate;
@JsonIgnoreProperties({ "hibernateLazyInitializer", "handler" })
@JsonIdentityInfo(generator = ObjectIdGenerators.PropertyGenerator.class, property = "clientId")

public class BillCommand {
    private Long clientId;
    private String direction;
    @JsonDeserialize(as = LocalDate.class)
    private LocalDate deliveryDate;
    private Long cost;

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public BillCommand(Long clientId, String direction, LocalDate deliveryDate, Long cost) {
        this.clientId = clientId;
        this.direction = direction;
        this.deliveryDate = deliveryDate;
        this.cost = cost;
    }

    public BillCommand() {
    }
}
