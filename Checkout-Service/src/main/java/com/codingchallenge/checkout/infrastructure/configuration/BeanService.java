package com.codingchallenge.checkout.infrastructure.configuration;

import com.codingchallenge.checkout.domain.port.repository.BillRepository;
import com.codingchallenge.checkout.domain.port.repository.OrderRepository;
import com.codingchallenge.checkout.domain.service.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BeanService {

	@Bean
	public CreateCheckoutService createCreateCheckoutService(OrderRepository orderRepository) {
		return new CreateCheckoutService(orderRepository);
	}
	@Bean
	public CreateBillService createBillService(BillRepository billRepository) {
		return new CreateBillService(billRepository);
	}
	@Bean
	public ConsultBillService consultBillService(BillRepository billRepository) {
		return new ConsultBillService(billRepository);
	}

}
