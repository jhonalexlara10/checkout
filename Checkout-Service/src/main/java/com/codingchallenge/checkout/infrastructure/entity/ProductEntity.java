package com.codingchallenge.checkout.infrastructure.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "PRODUCT")
public class ProductEntity {
	@Id
	@Column(name = "ID")
	private Long id;

	@Column(name = "QUANTITY")
	private Long quantity;

	@Column(name = "COST")
	private Long cost;

	@ManyToOne(fetch = FetchType.LAZY, optional = false)
	@JoinColumn(name = "order_entity_id", nullable = false)
	private OrderEntity order_entity;

	public ProductEntity(Long id, Long quantity, Long cost, OrderEntity order_entity) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.cost = cost;
		this.order_entity = order_entity;
	}

	public OrderEntity getOrder_entity() {
		return order_entity;
	}

	public Long getId() {
		return id;
	}

	public Long getQuantity() {
		return quantity;
	}

	public Long getCost() {
		return cost;
	}

	public ProductEntity() {
	}

}
