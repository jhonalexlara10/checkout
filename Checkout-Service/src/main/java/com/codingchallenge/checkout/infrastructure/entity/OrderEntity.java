package com.codingchallenge.checkout.infrastructure.entity;

import java.time.LocalDate;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.springframework.format.annotation.DateTimeFormat;

@Entity
@Table(name = "ORDER_ENTITY")
public class OrderEntity {

	@Id
	@Column(name = "CLIENT_ID")
	private Long clientId;

	@Column(name = "DIRECTION")
	private String direction;

	@Column(name = "DATE")
	@DateTimeFormat(pattern = "dd-MM-yyyy")
	private LocalDate date;

	@OneToMany(mappedBy = "order_entity", fetch = FetchType.LAZY, cascade = CascadeType.ALL)
	private List<ProductEntity> products;

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public void setProducts(List<ProductEntity> products) {
		this.products = products;
	}

	public Long getClientId() {
		return clientId;
	}

	public String getDirection() {
		return direction;
	}

	public LocalDate getDate() {
		return date;
	}

	public List<ProductEntity> getProducts() {
		return products;
	}

	public OrderEntity(Long clientId, String direction, LocalDate date, List<ProductEntity> products) {
		super();
		this.clientId = clientId;
		this.direction = direction;
		this.date = date;
		this.products = products;
	}

	public OrderEntity() {
	}

}
