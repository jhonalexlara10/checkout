package com.codingchallenge.checkout.infrastructure.controller;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.OrderCommand;
import com.codingchallenge.checkout.application.handler.*;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping(value = "/checkout")
public class CheckoutController {

	private final HandlerCheckout handlerCheckout;

	@Autowired
	public CheckoutController(HandlerCheckout handlerCheckout) {
		this.handlerCheckout = handlerCheckout;


	}
	@ApiOperation(value = "Checkout a new Order", nickname = "checkout", notes = "",response = BillCommand.class ,tags={ "checkout-controller", })
	@ApiResponses(value = {
			@ApiResponse(code = 200, message = "Successfully checkout a new order", response = BillCommand.class),
			@ApiResponse(code = 201, message = "Created"),
			@ApiResponse(code = 401, message = "You are not authorized to view the resource"),
			@ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
			@ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
			@ApiResponse(code = 500, message = "Application failed to process the request") })
	@PostMapping("/saveOrder")
	public BillCommand checkout(@RequestBody OrderCommand orderCommand) {
		return handlerCheckout.ejecutar(orderCommand);
	}

}
