package com.codingchallenge.checkout.infrastructure.adapter.repository;

import java.util.ArrayList;
import java.util.List;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.model.Bill;
import com.codingchallenge.checkout.domain.model.Order;
import com.codingchallenge.checkout.domain.model.Product;
import com.codingchallenge.checkout.domain.port.repository.BillRepository;
import com.codingchallenge.checkout.domain.port.repository.OrderRepository;
import com.codingchallenge.checkout.infrastructure.BillRepositoryJPA;
import com.codingchallenge.checkout.infrastructure.OrderRepositoryJPA;
import com.codingchallenge.checkout.infrastructure.entity.BillEntity;
import com.codingchallenge.checkout.infrastructure.entity.OrderEntity;
import com.codingchallenge.checkout.infrastructure.entity.ProductEntity;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Repository;


@Repository
public class RepositoryCheckoutH2 implements  OrderRepository, BillRepository {

	private final OrderRepositoryJPA orderRepositoryJPA;
	private final BillRepositoryJPA billRepositoryJPA;
	private ModelMapper modelMapper = new ModelMapper();

	public RepositoryCheckoutH2(OrderRepositoryJPA orderRepositoryJPA,BillRepositoryJPA billRepositoryJPA) {
		this.orderRepositoryJPA = orderRepositoryJPA;
		this.billRepositoryJPA=billRepositoryJPA;

	}

	@Override
	public void checkout(Order order) {

		OrderEntity orderEntity = new OrderEntity(order.getClientId(), order.getDirection(), order.getDate(), null);
		List<ProductEntity> products = new ArrayList<>();
		for (Product product : order.getProducts()) {
			products.add(new ProductEntity(product.getId(), product.getQuantity(), product.getCost(), orderEntity));
		}
		orderEntity.setProducts(products);
		orderRepositoryJPA.save(orderEntity);
	}

	@Override
	public void create(Bill bill) {
		BillEntity billEntity = new BillEntity(bill.getClientId(), bill.getDirection(), bill.getDeliveryDate(), bill.getCost());
		billRepositoryJPA.save(billEntity);
	}
	@Override
	public List<BillCommand> getBills() {
		List<BillEntity> billEntityList = billRepositoryJPA.findAll();
		List<BillCommand> listBills = new ArrayList<>();
		for (BillEntity billEntity : billEntityList) {
			BillCommand bill = modelMapper.map(billEntity, BillCommand.class);
			listBills.add(bill);
		}
		return listBills;
	}
}
