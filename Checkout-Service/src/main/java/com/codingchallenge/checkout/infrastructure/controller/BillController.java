package com.codingchallenge.checkout.infrastructure.controller;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.handler.HandlerBill;
import com.codingchallenge.checkout.application.handler.HandlerConsultBill;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/bill")
@Api(produces = "application/json", value = "Operations pertaining to  bills in the application")
public class BillController {
    private final HandlerBill handlerBill;
    private final HandlerConsultBill handlerConsultBill;

    @Autowired
    public BillController(HandlerBill handlerBill, HandlerConsultBill handlerConsultBill) {
        this.handlerBill = handlerBill;
        this.handlerConsultBill= handlerConsultBill;


    }

    @ApiOperation(value = "Get the bills", nickname = "getBills", notes = "",response = List.class,tags={ "bill-controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully: get the bills", response = List.class),
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Application failed to process the request") })
    @GetMapping("/getBills")
    public List<BillCommand> getBills() {
        return handlerConsultBill.execute();
    }

    @ApiOperation(value = "Create a new Bill", nickname = "saveBill", notes = "",tags={ "bill-controller", })
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "Successfully created a new donor"),
            @ApiResponse(code = 201, message = "Created"),
            @ApiResponse(code = 401, message = "You are not authorized to view the resource"),
            @ApiResponse(code = 403, message = "Accessing the resource you were trying to reach is forbidden"),
            @ApiResponse(code = 404, message = "The resource you were trying to reach is not found"),
            @ApiResponse(code = 500, message = "Application failed to process the request") })
    @PutMapping("/saveBill")
    public void saveBill(@RequestBody BillCommand billCommand) {
        this.handlerBill.execute(billCommand);
    }

}
