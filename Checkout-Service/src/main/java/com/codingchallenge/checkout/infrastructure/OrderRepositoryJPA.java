package com.codingchallenge.checkout.infrastructure;

import java.io.Serializable;

import com.codingchallenge.checkout.infrastructure.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepositoryJPA extends JpaRepository<OrderEntity, Serializable> {

}
