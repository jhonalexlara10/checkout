package com.codingchallenge.checkout.infrastructure;

import com.codingchallenge.checkout.infrastructure.entity.BillEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.io.Serializable;

@Repository
public interface BillRepositoryJPA extends JpaRepository<BillEntity, Serializable> {
}
