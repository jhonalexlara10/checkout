package com.codingchallenge.checkout.infrastructure.entity;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "Bill")
public class BillEntity {

    @Id
    @Column(name = "CLIENT_ID")
    private Long clientId;

    @Column(name = "DIRECTION")
    private String direction;

    @Column(name = "DATE")
    @DateTimeFormat(pattern = "dd-MM-yyyy")
    private LocalDate date;

    @Column(name = "COST")
    private Long cost;


    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public LocalDate getDate() {
        return date;
    }

    public void setDate(LocalDate date) {
        this.date = date;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public BillEntity(Long clientId, String direction, LocalDate date,Long cost ) {
        this.clientId=clientId;
        this.date= date;
        this.direction= direction;
        this.cost=cost;
    }
    public BillEntity(){

    }


}
