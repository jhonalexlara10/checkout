package com.codingchallenge.checkout.domain.model;

public class Product {
	
	private Long id;
	private Long quantity;
	private Long cost;
	
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Long getQuantity() {
		return quantity;
	}
	public void setQuantity(Long quantity) {
		this.quantity = quantity;
	}
	public Long getCost() {
		return cost;
	}
	public void setCost(Long cost) {
		this.cost = cost;
	}
	public Product(Long id, Long quantity, Long cost) {
		super();
		this.id = id;
		this.quantity = quantity;
		this.cost = cost;
	}
	
	
}
