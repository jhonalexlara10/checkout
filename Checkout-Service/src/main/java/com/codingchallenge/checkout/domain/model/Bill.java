package com.codingchallenge.checkout.domain.model;

import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

import java.time.LocalDate;

public class Bill {
    private Long clientId;
    private String direction;
    private LocalDate deliveryDate;
    private Long cost;

    public Long getClientId() {
        return clientId;
    }

    public void setClientId(Long clientId) {
        this.clientId = clientId;
    }

    public String getDirection() {
        return direction;
    }

    public void setDirection(String direction) {
        this.direction = direction;
    }

    public LocalDate getDeliveryDate() {
        return deliveryDate;
    }

    public void setDeliveryDate(LocalDate deliveryDate) {
        this.deliveryDate = deliveryDate;
    }

    public Long getCost() {
        return cost;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Bill(Long clientId, String direction, LocalDate deliveryDate, Long cost) {
        this.clientId = clientId;
        this.direction = direction;
        this.deliveryDate = deliveryDate;
        this.cost = cost;
    }
}
