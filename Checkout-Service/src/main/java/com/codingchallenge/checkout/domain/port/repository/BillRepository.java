package com.codingchallenge.checkout.domain.port.repository;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.model.Bill;
import com.codingchallenge.checkout.domain.model.Order;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BillRepository {
    void create(Bill bill);
    List<BillCommand> getBills();


}
