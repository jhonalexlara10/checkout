package com.codingchallenge.checkout.domain.model;

import java.time.LocalDate;
import java.util.List;

public class Order {

	private Long clientId;
	private LocalDate date;
	private String direction;
	private List<Product> products;

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public String getDirection() {
		return direction;
	}

	public Order(Long clientId, LocalDate date, String direction, List<Product> products) {
		super();
		this.clientId = clientId;
		this.date = date;
		this.direction = direction;
		this.products = products;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public List<Product> getProducts() {
		return products;
	}

	public void setProducts(List<Product> products) {
		this.products = products;
	}
}
