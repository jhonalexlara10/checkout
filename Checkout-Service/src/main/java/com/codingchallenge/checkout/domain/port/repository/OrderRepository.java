package com.codingchallenge.checkout.domain.port.repository;


import com.codingchallenge.checkout.domain.model.Order;
import org.springframework.stereotype.Repository;

@Repository
public interface OrderRepository {
	void checkout(Order order);

}
