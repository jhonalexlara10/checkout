package com.codingchallenge.checkout.domain.model;

import java.time.LocalDate;

public class Logistic {
	private Long clientId;
	private String direction;
	private LocalDate date;
	private LocalDate deliveredDate;

	public Logistic(Long clientId, String direction, LocalDate date, LocalDate deliveredDate) {
		super();
		this.clientId = clientId;
		this.direction = direction;
		this.date = date;
		this.deliveredDate = deliveredDate;
	}

	public Logistic() {
	}

	public Long getClientId() {
		return clientId;
	}

	public void setClientId(Long clientId) {
		this.clientId = clientId;
	}

	public String getDirection() {
		return direction;
	}

	public void setDirection(String direction) {
		this.direction = direction;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public LocalDate getDeliveredDate() {
		return deliveredDate;
	}

	public void setDeliveredDate(LocalDate deliveredDate) {
		this.deliveredDate = deliveredDate;
	}

}
