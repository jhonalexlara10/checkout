package com.codingchallenge.checkout.domain.service;

import com.codingchallenge.checkout.domain.model.Bill;
import com.codingchallenge.checkout.domain.port.repository.BillRepository;
import com.codingchallenge.checkout.domain.port.repository.OrderRepository;

public class CreateBillService {
    private BillRepository billRepository;

    public CreateBillService(BillRepository billRepository) {

        this.billRepository = billRepository;
    }

    public void execute(Bill bill) {
        this.billRepository.create(bill);
    }
}
