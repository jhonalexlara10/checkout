package com.codingchallenge.checkout.domain.service;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.port.repository.BillRepository;

import java.util.List;

public class ConsultBillService {
    private BillRepository billRepository;

    public ConsultBillService(BillRepository billRepository) {

        this.billRepository = billRepository;
    }

    public List<BillCommand> execute() {
        return this.billRepository.getBills();
    }

}
