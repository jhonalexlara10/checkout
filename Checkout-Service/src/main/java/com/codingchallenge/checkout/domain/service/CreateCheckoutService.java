package com.codingchallenge.checkout.domain.service;

import java.time.LocalDate;

import com.codingchallenge.checkout.application.command.BillCommand;
import com.codingchallenge.checkout.application.command.LogisticCommand;
import com.codingchallenge.checkout.domain.model.Order;
import com.codingchallenge.checkout.domain.model.Product;
import com.codingchallenge.checkout.domain.port.repository.BillRepository;
import com.codingchallenge.checkout.domain.port.repository.OrderRepository;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public class CreateCheckoutService {
	private OrderRepository orderRepository;

	public CreateCheckoutService(OrderRepository orderRepository) {
		this.orderRepository = orderRepository;
	}

	public BillCommand execute(Order order) {
		Long totalCost = 0L;

		// this calculate the cost bill
		for (Product product : order.getProducts()) {
			totalCost += product.getCost() * product.getQuantity();
		}
		// For the call logistic service
		RestTemplate restTemplate = new RestTemplate();
        LogisticCommand logistic = new LogisticCommand(order.getClientId(), order.getDirection(), order.getDate(), null);

        // Evidence of call logistic service

        System.out.println("Logistic info: " + logistic.getClientId());
		System.out.println("Logistic info: " + logistic.getDirection());
		System.out.println("Logistic info: " + logistic.getDate());
		LocalDate result=null;
		// URL Logistic service
		// logistic-server is the host in the docker cloud
		String url="http://logistic-server:8080/logistic/saveLogistic";
		try {
            HttpHeaders headers = new HttpHeaders();
            headers.setContentType(MediaType.APPLICATION_JSON);
            headers.add("Accept", "*/*");
            HttpEntity<LogisticCommand> entity = new HttpEntity<>(logistic, headers);
			result = restTemplate.postForObject(url, entity, LocalDate.class);
			System.out.println("This return: " + result);
		}catch (Exception e){
			System.out.println("Service Fall "+url);
            System.out.println("This Fall "+e.getMessage());
            System.out.println("Cause "+e.getCause());
		}
		BillCommand billCommand= new BillCommand(order.getClientId(), order.getDirection(), result, totalCost);
		// URL Logistic service
		// logistic-server is the host in the docker cloud
		String urlBill="http://checkout-server:8080/bill/saveBill";
		HttpHeaders headers = new HttpHeaders();
		headers.setContentType(MediaType.APPLICATION_JSON);
		headers.add("Accept", "*/*");
		HttpEntity<BillCommand> entityBill = new HttpEntity<>(billCommand, headers);
		restTemplate.put(urlBill, entityBill);
		this.orderRepository.checkout(order);
		return billCommand;
	}
}
