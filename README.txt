Api description

Checkout sevice will receive Orders in a REST Endpoint. When checkout service is invoked, 2 operations will be
triggered: 
- Bill the sum of all products to user in Bill service, and return bill with the total cost
- Call Logistic service to create a sent order and return the delivery date (the delivery is in 5 days) 


Tools

Docker compose
Compose is the tool for defining and running multi-container Docker applications

SpringBoot
Maven
Docker
Swagger
H2

I select hexagonal arqchitecture to these reasons:

The Ports and Adapters are replaceable.
Separation of the different rates of change. 
Independent of external services.
Easier to test in isolation.


 For compile the proyects:
 
 ./mvnw clean package // in ./Checkout-Service/ and ./Logistics-Service/
 
 For execute the services:
 
 if required create volume run the command: docker volume create --name=spring-cloud-config-repo

 docker-compose up --build //This executes the file docker-compose.yml 
 
 For view the services:
 
 http://localhost:8888/swagger-ui.html logistic 
 the service returns the  delivery date
 
 http://localhost:8081/swagger-ui.html checkout
 
 
 With this you test the api
 
 The principal service is http://localhost:8081/checkout/saveOrder this is a post service
 JSON example
 {
  "clientId": 1,
  "date": "2020-07-01",
  "direction": "Av 13 #35",
  "products": [
    {
      "cost": 12000,
      "id": 1,
      "quantity": 10
    },
    {
      "cost": 1100,
      "id": 2,
      "quantity": 15
    }
  ]
}
this order has 2 products, the service returns the bill 
	

 
 
 
